#ifndef NFC_INTF_MANAGER_H
#define NFC_INTF_MANAGER_H

#include <stdint.h>

#define                             DEVICE_NAME_LEN     16                  /**< Length of BLE advertised name */

int init_nfc_intf_manager(void);
void nfc_interrupt_occurred(void);

int nfc_read(uint8_t *buffer, uint32_t num_bytes);
int nfc_write(uint8_t *buffer, uint32_t num_bytes);

#endif