#ifndef BOARD_DEFINES_H
#define BOARD_DEFINES_H

//I2C defines
#define                             I2C_DEVICE_NAME         "I2C_0"         /**< I2C device name for binding */
#define                             NFC_I2C_ADDR            8               /**< Address of NFC device on the I2C */

//UART defines
#define                             UART_DEVICE_NAME        "UART_0"        /**< UART device name for binding */
#define                             UART_BAUD_RATE          115200          /**< UART baud rate */

//GPIO defines
#define                             GPIO_DEVICE_NAME        "GPIO_0"        /**< GPIO device name for binding */
#define                             NFC_INT_PIN_IN          28              /**< Input interrupt for NFC to indicate data available */
#define                             UART_INT_PIN_OUT        30              /**< Output interrupt to manage UART communication */
#define                             UART_INT_PIN_IN         31              /**< Input interrupt to manage UART communication */

#endif