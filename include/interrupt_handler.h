#ifndef INTERRUPT_HANDLER_H
#define INTERRUPT_HANDLER_H

#include <kernel.h>

//Enumeration of interrupts handled by the NRF52
typedef enum { NFC_IN, UART_IN, UART_OUT, MAX_INTERRUPT_TYPE } interrupt_type_t;

void        init_interrupt_handler(void);
void        handle_interrupt(interrupt_type_t type);

#endif