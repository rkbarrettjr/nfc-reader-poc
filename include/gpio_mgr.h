#ifndef GPIO_MGR_H
#define GPIO_MGR_H

#include <drivers/gpio.h>

void        init_gpio_mgr(void);
void        set_gpio_callback(int gpio_pin_num, struct gpio_callback *data, void *cb);
void        enable_gpio_interrupt(int gpio_pin_num);
void        set_gpio_state(int gpio_pin_num, int state);

#endif