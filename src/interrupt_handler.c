#include "interrupt_handler.h"

#include "logging/log.h"
/** @brief Declare logger */
LOG_MODULE_DECLARE(NfcReader, CONFIG_LOG_MAX_LEVEL); 

#include "nfc_intf_manager.h"

#define                                 WORKER_STACK_SIZE 512           /**< Size of worker queue stack */
#define                                 WORKER_PRIORITY 5               /**< Thread priority of worker queue */

/** @brief Container for functions operating in the worker thread */
struct interrupt_info_t {
    struct k_work work;             /**< object required by worker queue */
    interrupt_type_t type;          /**< type of interrupt that occurred */
};

/** @brief Define the worker queue stack */
K_THREAD_STACK_DEFINE(m_stack_area, WORKER_STACK_SIZE);
static struct k_work_q                  m_work_q;                       /**< Worker queue */
static struct interrupt_info_t          m_interrupt_info;               /**< Struct passed from interrupt handlers to worker queue */

//local helper functions
static void interrupt_worker(struct k_work *item);

/**@brief Interrupt Handler Initialization
 *
 * @details Creates the worker queue to handle device interrupts in a
 *          lower priority thread.
 */
void init_interrupt_handler(void) {
    //setup workqueue to offload interrupts
    k_work_q_start(&m_work_q, m_stack_area, WORKER_STACK_SIZE, WORKER_PRIORITY);
    //assign function to perform work
    k_work_init(&m_interrupt_info.work, interrupt_worker);
}

/**@brief Interrupt Handler Function
 *
 * @details Function invoked from within an interrupt handler.
 * 
 * @param[in] type interrupt type
 */
void handle_interrupt(interrupt_type_t type) {
    m_interrupt_info.type = type;
    //offload task to the worker queue thread
    k_work_submit(&m_interrupt_info.work);
}

//Worker operating on a lower priority thread
static void interrupt_worker(struct k_work *item) {
    struct interrupt_info_t *int_info = CONTAINER_OF(item, struct interrupt_info_t, work);
    LOG_DBG("Interrupt worker: %d", int_info->type);

    //process each interrupt
    switch (int_info->type) {
        case NFC_IN:
            nfc_interrupt_occurred();
            break;
        case UART_IN:
            LOG_DBG("UART IN Interrupt detected");
            //TODO
            break;
        case UART_OUT:
            LOG_DBG("UART OUT Interrupt detected");
            //TODO
            break;
        default:
            break;
    }
}