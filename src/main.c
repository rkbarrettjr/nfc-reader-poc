#include <zephyr.h>

#include <logging/log.h>
/** @brief Register logger */
LOG_MODULE_REGISTER(NfcReader, CONFIG_LOG_MAX_LEVEL);

#include "nfc_intf_manager.h"
#include "gpio_mgr.h"
#include "interrupt_handler.h"

void main(void) {
    LOG_DBG("Start NFC Reader initialization...");

    init_interrupt_handler();
    init_gpio_mgr();
    
    init_nfc_intf_manager();

    while (1) {
        k_sleep(K_MSEC(100));
    }
}
