#include "gpio_mgr.h"

#include "logging/log.h"
/** @brief Declare logger */
LOG_MODULE_DECLARE(FallMonNrf, CONFIG_LOG_MAX_LEVEL); 

#include "board_defines.h"

/** @brief Initialize GPIO Manager
 *
 * @details Function will setup the GPIO direction and state for the board.
 */
void init_gpio_mgr(void) {
    const struct device *dev = device_get_binding(GPIO_DEVICE_NAME);

    //disable interrupts while configuring GPIOs
    gpio_pin_interrupt_configure(dev, NFC_INT_PIN_IN, GPIO_INT_DISABLE);

    //configure GPIOs
    gpio_pin_configure(dev, NFC_INT_PIN_IN, (GPIO_INPUT | GPIO_ACTIVE_LOW  | GPIO_PULL_UP));
}

/** @brief Adds GPIO Callback Function
 *
 * @details Function will configure a callback function to be invoked
 *          when the given GPIO pin is asserted.
 *
 * @param[in] gpio_pin_num pin number on the board
 * @param[in] data callback data
 * @param[in] cb callback funciton to invoke when GPIO asserted
 */
void set_gpio_callback(int gpio_pin_num, struct gpio_callback *data, void *cb) {
    const struct device *dev = device_get_binding(GPIO_DEVICE_NAME);
    gpio_init_callback(data, cb, BIT(gpio_pin_num));
    gpio_add_callback(dev, data);
}

/** @brief Enable GPIO Interrupt
 *
 * @details Function will enable interrupts for the given GPIO.
 *
 * @param[in] gpio_pin_num pin number on the board
 */
void enable_gpio_interrupt(int gpio_pin_num) {
    const struct device *dev = device_get_binding(GPIO_DEVICE_NAME);
    gpio_pin_interrupt_configure(dev, gpio_pin_num, GPIO_INT_ENABLE);
}

/** @brief Set Ouput GPIO State
 *
 * @details Function will assert / de-assert the given GPIO.
 *
 * @param[in] gpio_pin_num pin number on the board
 * @param[in] state 1 to assert, 0 to de-assert
 */
void set_gpio_state(int gpio_pin_num, int state) {
    const struct device *dev = device_get_binding(GPIO_DEVICE_NAME);
    gpio_pin_set(dev, gpio_pin_num, state);
}