/** @file
 * Copyright (c) 2021, Bold Type
 * All rights reserved.
 *
 * @defgroup soc_fm_ble_nfc_intf_manager nfc_intf_manager.c
 * @{
 * @ingroup soc_fm_ble
 * @brief NFC Interface Manager
 *
 * Manages the I2C between the BLE (NRF52832) and the NFC reader
 * (CLRC66303). A separate GPIO will be uses by the NFC reader to
 * indicate to the BLE chip that it has data available.
 */

#include "nfc_intf_manager.h"

#include "logging/log.h"
/** @brief Declare logger */
LOG_MODULE_DECLARE(NfcReader, CONFIG_LOG_MAX_LEVEL); 
#include <drivers/i2c.h>
#include <drivers/gpio.h>

#include "board_defines.h"
#include "interrupt_handler.h"

#include "gpio_mgr.h"

static const struct device *        m_i2c_bus;                          /**< Binding for I2C to NFC reader */
//static const struct device *        m_nfc_gpio;                         /**< Binding for GPIO used as input interrupt from NFC reader */
static bool                         m_nfc_read_ready = false;           /**< Tracks when NFC has indicated NFC data is ready to be read */
static struct gpio_callback m_cb_data;

//local helper functions
static int init_i2c_bus(void);
static void nfc_gpio_cb(const struct device *dev, struct gpio_callback *gpio_cb, uint32_t pins);

/** @brief NFC interface initialization
 *
 * @details Function will initialize the I2C interface between
 * the NRF and the NFC reader.
 *
 * @return 0 on SUCCESS
 */
int init_nfc_intf_manager(void) {
    LOG_DBG("Initializing NFC Interface Manager...");

    int ret = init_i2c_bus();
    if (ret == 0) {
        set_gpio_callback(NFC_INT_PIN_IN, &m_cb_data, nfc_gpio_cb);
        enable_gpio_interrupt(NFC_INT_PIN_IN);
    }
    return ret;
}

int nfc_read(uint8_t *buffer, uint32_t num_bytes) {
    int ret = i2c_read(m_i2c_bus, buffer, num_bytes, NFC_I2C_ADDR);
    if (ret != 0) {
        LOG_ERR("Failure reading I2C");
    }
    return ret;
}


int nfc_write(uint8_t *buffer, uint32_t num_bytes) {
    int ret = i2c_write(m_i2c_bus, buffer, num_bytes, NFC_I2C_ADDR);
    if (ret != 0) {
        LOG_ERR("Failure writing I2C");
    }
    return ret;
}


static void print_ble_info(char *premsg_str, char *info, size_t size) {
	char tmp[size + 1];
	memcpy(tmp, info, size);
	tmp[size] = '\0';
	printk("%s: %s\n", premsg_str, tmp);
}

/**@brief Process NFC Interrupt
 *
 * @details Function called from a worker thread to handle when the NFC reader
 *          asserts the GPIO. It will read data from the NFC reader and process it.
 */
void nfc_interrupt_occurred(void) {
    LOG_DBG("NFC Interrupt detected");

    uint8_t write_buf[1]; 
    write_buf[0] = '0';
    uint8_t advertised_name[DEVICE_NAME_LEN];
    int ret = i2c_write_read(m_i2c_bus, NFC_I2C_ADDR, write_buf, 1, advertised_name, DEVICE_NAME_LEN);
    if (ret == 0) {
        if (advertised_name != NULL) {
            //TODO process data here or send to STM32
            print_ble_info("Adv name: ", advertised_name, DEVICE_NAME_LEN);
            if (ret != 0) {
                LOG_ERR("Failure to start BLE scan");
            }
        } else {
            LOG_ERR("Advertised name is NULL");
        }
    } else {
        LOG_ERR("Fail on read");
    }

    m_nfc_read_ready = false;
    //turn the interrupt back on
    enable_gpio_interrupt(NFC_INT_PIN_IN);
    if (ret != 0) {
        LOG_ERR("GPIO interrupt could not be re-enabled");
        //TODO fatal error
    }
}

//Initialize the I2C
static int init_i2c_bus(void) {
    int ret = 0;
    m_i2c_bus = device_get_binding(I2C_DEVICE_NAME);
    if (!m_i2c_bus) {
        LOG_ERR("I2C Device binding not found");
        ret = -1;
    } else {
        uint32_t i2c_cfg = I2C_SPEED_SET(I2C_SPEED_STANDARD) | I2C_MODE_MASTER;
        if (i2c_configure(m_i2c_bus, i2c_cfg)) {
            LOG_ERR("I2C configuration failed");
            ret = -1;
        }
    }
    return ret;
}

//Callback invoked when NFC triggers a GPIO interrupt
static void nfc_gpio_cb(const struct device *dev, struct gpio_callback *gpio_cb, uint32_t pins) {
    if (!m_nfc_read_ready) {
        LOG_DBG("Got NFC callback");
        //disable the gpio interrupt while reading NFC data
        gpio_pin_interrupt_configure(dev, NFC_INT_PIN_IN, GPIO_INT_DISABLE);
        m_nfc_read_ready = true;
        //call handle interrupt to move NFC reading to a lower priority thread
        handle_interrupt(NFC_IN);
    }
}

/**
 * @}
 */